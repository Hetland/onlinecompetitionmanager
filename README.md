Online competition manager


Models
* Competitor

Contexts
* CompetitionManagerOnlineContext       ->  Can do CRUD operations for Competitor model

Controllers
* CompetitorsController                 ->  Contains all CRUD controls for Competitors

Views
* Home                                  ->  Initial views, contains link to an overview of all contestants
* Competitors                           ->  Views for all Competitor CRUD operations