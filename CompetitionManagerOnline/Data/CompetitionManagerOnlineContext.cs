﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CompetitionManagerOnline.Models;

namespace CompetitionManagerOnline.Data
{
    public class CompetitionManagerOnlineContext : DbContext
    {
        public CompetitionManagerOnlineContext (DbContextOptions<CompetitionManagerOnlineContext> options)
            : base(options)
        {
        }

        public DbSet<CompetitionManagerOnline.Models.Competitor> Competitor { get; set; }
    }
}
