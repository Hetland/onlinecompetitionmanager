﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CompetitionManagerOnline.Models
{
    public class Competitor
    {
        [Key]
        public int Id {get;set;}

        [Required]
        [MaxLength(40)]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        [MaxLength(60)]
        [Required]
        public string Details { get; set; }

        [Required]
        public int Age { get; set; }
        public float? HeightInMeters { get; set; }
        public int? WeightKg { get; set; }

        public Competitor()
        {
            Name = "Stick Strongman";
            Details = @"Very strong, very large";
            Age = 28;
            HeightInMeters = 2.1f;
            WeightKg = 120;
        }
    }
}
